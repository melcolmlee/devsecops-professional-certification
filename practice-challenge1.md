# Challenge 1
We tried to implement all the best practices we have learned in the course
1. Tested the tools locally before embedding in the pipeline
2. Ensured the scans finish within 10 minutes
3. Ensured they each run in their own jobs
4. We saved the output in a file
5. We didn’t fail the builds

## Task 1 : Test the tools locally
Implement SCA, SAST, DAST for the django.nv project
As per the best practices, we need to test DevSecOps tools locally before embedding them into CI/CD pipeline.

### Clone Codes
```bash
git clone http://gitlab-ce-xxxx.lab.practical-devsecops.training/root/django-nv.git
```

### Software Component Analysis (SCA)
#### Fontend (nodejs)
```bash
npm install -g retire # Install retirejs npm package
retire --outputformat json --outputpath retirejs-report.json --severity high
```
#### Backend (Python)
```bash
docker run --rm -v $(pwd):/src hysnsec/safety check -r requirements.txt --json > oast-results.json
```

### Static Application Security Testing (SAST)
#### Secrets-scanning
```bash 
docker run --rm -v $(pwd):/src hysnsec/trufflehog filesystem --directory=/src --json | tee trufflehog-output.json
```
#### Code Analysis
```bash
docker run --user $(id -u):$(id -g) --rm -v $(pwd):/src hysnsec/bandit -r /src -f json -o /src/bandit-output.json
```

### Dynamic Application Security Testing (DAST)
#### SSL Scan
```bash
docker run --rm -v $(pwd):/tmp hysnsec/sslyze prod-xxxx.lab.practical-devsecops.training:443 --json_out /tmp/sslyze-output.json
```
#### Nmap
```bash
docker run --rm -v $(pwd):/tmp hysnsec/nmap prod-0ppi2mfc -oX /tmp/nmap-output.xml
```
#### ZAP Baseline
```bash 
docker run --user $(id -u):$(id -g) -w /zap -v $(pwd):/zap/wrk:rw --rm softwaresecurityproject/zap-stable:2.13.0 zap-baseline.py -t https://prod-xxxx.lab.practical-devsecops.training -J zap-output.json    
```


## Task 2 : Embed these tests in CI/CD pipeline
```yaml
image: docker:20.10

services:
  - docker:dind

stages:
  - build
  - test
  - release
  - preprod
  - integration
  - prod

build:
  stage: build
  image: python:3.6
  before_script:
   - pip3 install --upgrade virtualenv
  script:
   - virtualenv env
   - source env/bin/activate
   - pip install -r requirements.txt
   - python manage.py check

test:
  stage: test
  image: python:3.6
  before_script:
   - pip3 install --upgrade virtualenv
  script:
   - virtualenv env
   - source env/bin/activate
   - pip install -r requirements.txt
   - python manage.py test taskManager

integration:
  stage: integration
  script:
    - echo "This is an integration step"
    - exit 1
  allow_failure: true # Even if the job fails, continue to the next stages

prod:
  stage: prod
  script:
    - echo "This is a deploy step."
  when: manual # Continuous Delivery
```

### With all tests embedded
```yaml
image: docker:20.10

services:
  - docker:dind

stages:
  - build
  - test
  - release
  - preprod
  - integration
  - prod

build:
  stage: build
  image: python:3.6
  before_script:
   - pip3 install --upgrade virtualenv
  script:
   - virtualenv env
   - source env/bin/activate
   - pip install -r requirements.txt
   - python manage.py check

test:
  stage: test
  image: python:3.6
  before_script:
   - pip3 install --upgrade virtualenv
  script:
   - virtualenv env
   - source env/bin/activate
   - pip install -r requirements.txt
   - python manage.py test taskManager

# Software Component Analysis
sca-frontend:
  stage: build
  image: node:alpine3.10
  script:
    - npm install
    - npm install -g retire # Install retirejs npm package.
    - retire --outputformat json --outputpath retirejs-report.json --severity high
  artifacts:
    paths: [retirejs-report.json]
    when: always # What is this for?
    expire_in: one week
  allow_failure: true #<--- allow the build to fail but don't mark it as such  

sca-backend:
  stage: build
  script:
    - docker pull hysnsec/safety
    - docker run --rm -v $(pwd):/src hysnsec/safety check -r requirements.txt --json > oast-results.json
  artifacts:
    paths: [oast-results.json]
    when: always # What does this do?
  allow_failure: true #<--- allow the build to fail but don't mark it as such

# Git Secrets Scanning
secrets-scanning:
  stage: build
  script:
    - docker run -v $(pwd):/src --rm hysnsec/trufflehog filesystem --directory=/src --json | tee trufflehog-output.json
  artifacts:
    paths: [trufflehog-output.json]
    when: always  # What is this for?
    expire_in: one week
  allow_failure: true

# Static Application Security Testing
sast:
  stage: build
  script:
    - docker pull hysnsec/bandit  # Download bandit docker container
    # Run docker container, please refer docker security course, if this doesn't make sense to you.
    - docker run --user $(id -u):$(id -g) -v $(pwd):/src --rm hysnsec/bandit -r /src -f json -o /src/bandit-output.json
  artifacts:
    paths: [bandit-output.json]
    when: always
  allow_failure: true   #<--- allow the build to fail but don't mark it as such

# Dynamic Application Security Testing
nikto:
  stage: integration
  script:
    - docker pull hysnsec/nikto
    - docker run --rm -v $(pwd):/tmp hysnsec/nikto -h prod-0ppi2mfc -o /tmp/nikto-output.xml
  artifacts:
    paths: [nikto-output.xml]
    when: always

sslscan:
  stage: integration
  script:
    - docker pull hysnsec/sslyze
    - docker run --rm -v $(pwd):/tmp hysnsec/sslyze prod-0ppi2mfc.lab.practical-devsecops.training:443 --json_out /tmp/sslyze-output.json
  artifacts:
    paths: [sslyze-output.json]
    when: always

nmap:
  stage: integration
  script:
    - docker pull hysnsec/nmap
    - docker run --rm -v $(pwd):/tmp hysnsec/nmap prod-0ppi2mfc -oX /tmp/nmap-output.xml
  artifacts:
    paths: [nmap-output.xml]
    when: always

zap-baseline:
  stage: integration
  script:
    - docker pull softwaresecurityproject/zap-stable:2.13.0
    - docker run --user $(id -u):$(id -g) --rm -v $(pwd):/zap/wrk:rw softwaresecurityproject/zap-stable:2.13.0 zap-baseline.py -t https://prod-0ppi2mfc.lab.practical-devsecops.training -J zap-output.json
  after_script:
    - docker rmi softwaresecurityproject/zap-stable:2.13.0  # clean up the image to save the disk space
  artifacts:
    paths: [zap-output.json]
    when: always # What does this do?
  allow_failure: true
```