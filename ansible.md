# Ansible

## Install ansible
```bash
pip3 install ansible==6.4.0 ansible-lint==6.8.1
```

## Populate the inventory file
```bash
vi inventory.ini
```

```ini
# DevSecOps Studio Inventory
[devsecops]
devsecops-box-hsjzg1g1

[prod]
prod-hsjzg1g1
```

## Ensure SSH’s yes/no prompt is not shown while running ansible commands
```bash
ssh-keyscan -t rsa devsecops-box-xxxx >> ~/.ssh/known_hosts
ssh-keyscan -t rsa prod-xxxx devsecops-box-xxxx >> ~/.ssh/known_hosts
```

## List which hosts in our inventory matches a supplied group name
```bash
ansible -i inventory.ini prod --list-hosts
```

## Use the ping module to ping all hosts
```bash
ansible -i inventory.ini all -m ping
```

## Use apt module to install ntp service on production server
```bash
ansible -i inventory.ini  prod -m apt -a "name=ntp state=present"
```

## Find bash version installed on all machines
```bash
ansible -i inventory.ini all -m command -a "bash --version"
```

## Ansible doc command to find shell module
```bash
ansible-doc -l | grep shell
```

## Run uptime command on production server using shell module
```bash
ansible -i inventory.ini prod -m shell -a "uptime"
```

## Ansible ad-hoc command to copy file /root/notes into all remote machines (sandbox and production) to the destination directory /root
```bash
ansible -i inventory.ini all -m copy -a "src=/root/notes dest=/root"
```

## Sample playbook to install firewalld
```bash
vi playbook.yml
```

```yml
---
- name: Example playbook to install firewalld
  hosts: prod
  remote_user: root
  become: yes
  gather_facts: no
  vars:
    state: present

  tasks:
  - name: ensure firewalld is at the latest version
    apt:
      name: firewalld
```

Run playbook against all machines in inventory.ini
```bash
ansible-playbook -i inventory.ini playbook.yml
```

## Sample playbook with secfigo.terraform role to install the Terraform utility
```bash
vi playbook.yml
```

```yml
---
- name: Example playbook to install Terraform using ansible role.
  hosts: prod
  remote_user: root
  become: yes

  roles:
    - secfigo.terraform
```

Install secfigo.terraform role using ansible-galaxy
```bash
ansible-galaxy role install secfigo.terraform
```

Run playbook against all machines in inventory.ini to install Terraform utility
```bash
ansible-playbook -i inventory.ini playbook.yml
```

## Sample playbook with dev-sec.os-hardening role to harden a target server
```bash
vi ansible-hardening.yml
```

```yml
---
- name: Playbook to harden Ubuntu OS.
  hosts: prod
  remote_user: root
  become: yes

  roles:
    - dev-sec.os-hardening
```

Install dev-sec.os-hardening role from ansible-galaxy
```bash
ansible-galaxy install dev-sec.os-hardening
```

Run playbook against all machines in inventory.ini to harden them
```bash
ansible-playbook -i inventory.ini hardening.yml
```

## Create ansible config file to change log output to JSON
```bash
vi ansible.cfg
```

```ini
[defaults]
log_path=./ansible.json
stdout_callback=json
```