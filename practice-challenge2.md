# Challenge 2
We tried to implement all the best practices we have learned in the course
1. Tested the tools locally before embedding in the pipeline
2. Ensured the scans finish within 10 minutes
3. Ensured they each run in their own jobs
4. We saved the output in a file
5. We didn’t fail the builds

## Task 1 : Harden the production machine

### Create inventory.ini
```bash
cat > inventory.ini <<EOL
# DevSecOps Studio Inventory
[prod]
prod-0ppi2mfc

EOL
```
### Download Role
```bash
ansible-galaxy install dev-sec.os-hardening
```

### Create playbook
```bash
cat > ansible-hardening.yml <<EOL
---
- name: Playbook to harden Ubuntu OS.
  hosts: prod
  remote_user: root
  become: yes

  roles:
    - dev-sec.os-hardening

EOL
```

### Run playbook
```bash
ansible-playbook -i inventory.ini ansible-hardening.yml
```

## Task 2 : Ensure it stays compliant with linux-baseline Inspec Profile
/share directory should be used when using hysnsec/inspec image. Because it’s a custom image adding another directory would not work when you are saving the inspec output.

```bash
docker run --rm -v ~/.ssh:/root/.ssh -v $(pwd):/share hysnsec/inspec exec https://github.com/dev-sec/linux-baseline.git -t ssh://root@$DEPLOYMENT_SERVER -i /root/.ssh/id_rsa --chef-license accept --reporter json:/share/inspec-output.json
```

Remediate control failures from inspec
- File /etc/cron.daily is expected not to be readable by group 
- File /etc/cron.daily is expected not to be readable by other 
- File /etc/cron.d is expected not to be readable by group 
- File /etc/cron.d is expected not to be readable by other
- Mount /dev options is expected to include "noexec"
```bash
ssh root@prod-xxxx
chown root:root /etc/cron.daily
chmod og-rwx /etc/cron.daily
chown root:root /etc/cron.d
chmod og-rwx /etc/cron.d
vi /etc/fstab
mount -o remount,noexec /dev
```

Add line to /etc/fstab
```
tmpfs /dev tmpfs defaults,nodev,nosuid,noexec 0 0
```

## Task 3 : Embed these tests in CI/CD pipeline
Make sure you have added the necessary variables into your project (Settings > CI/CD) such as $DEPLOYMENT_SERVER_SSH_PRIVKEY and $DEPLOYMENT_SERVER.

```yaml
image: docker:20.10

services:
  - docker:dind

stages:
  - build
  - test
  - release
  - preprod
  - integration
  - prod

# Infrastructure as Code
# PLEASE ENSURE YOU HAVE SETUP THE ENVIRONMENT VARIABLES AND NEEDED FILES APPROPRIATELY
ansible-hardening:
  stage: prod
  image: willhallonline/ansible:2.9-ubuntu-18.04
  before_script:
    - mkdir -p ~/.ssh
    - echo "$DEPLOYMENT_SERVER_SSH_PRIVKEY" | tr -d '\r' > ~/.ssh/id_rsa
    - chmod 600 ~/.ssh/id_rsa
    - eval "$(ssh-agent -s)"
    - ssh-add ~/.ssh/id_rsa
    - ssh-keyscan -t rsa $DEPLOYMENT_SERVER >> ~/.ssh/known_hosts
  script:
    - echo -e "[prod]\n$DEPLOYMENT_SERVER" >> inventory.ini
    - ansible-galaxy install dev-sec.os-hardening
    - ansible-playbook -i inventory.ini ansible-hardening.yml

# Compliance as Code
# PLEASE ENSURE YOU HAVE SETUP THE ENVIRONMENT VARIABLES AND NEEDED FILES APPROPRIATELY
inspec:
  stage: prod
  only:
    - "main"
  environment: production
  before_script:
    - mkdir -p ~/.ssh
    - echo "$DEPLOYMENT_SERVER_SSH_PRIVKEY" | tr -d '\r' > ~/.ssh/id_rsa
    - chmod 600 ~/.ssh/id_rsa
    - eval "$(ssh-agent -s)"
    - ssh-add ~/.ssh/id_rsa
    - ssh-keyscan -t rsa $DEPLOYMENT_SERVER >> ~/.ssh/known_hosts
  script:
    - docker run --rm -v ~/.ssh:/root/.ssh -v $(pwd):/share hysnsec/inspec exec https://github.com/dev-sec/linux-baseline.git -t ssh://root@$DEPLOYMENT_SERVER -i ~/.ssh/id_rsa --chef-license accept --reporter json:inspec-output.json
  artifacts:
    paths: [inspec-output.json]
    when: always
```