# OWASP Dependency Check

## Run dependency check (Docker)
```bash
docker run --rm \
  --volume $(pwd):/src \
  --volume $(pwd)/data:/usr/share/dependency-check/data \
  owasp/dependency-check \
  --scan /src \
  --format "JSON" \
  --project "Webgoat" \
  --out /src \
  --nvdApiKey "########-####-####-####-############" \
  --nvdApiDelay 5000 #Time in milliseconds to wait between downloading from the NVD, increases chance of not failing (optional)
```
Important to provide data diectory, to prevent re-downloading NVD database

To fail on CVSS more than 4
```bash
docker run --rm \
  --volume $(pwd):/src \
  --volume $(pwd)/data:/usr/share/dependency-check/data \
  owasp/dependency-check \
  --scan /src \
  --format "JSON" \
  --out /src \
  --nvdApiKey "########-####-####-####-############" \
  --project "Webgoat" \
  --failOnCVSS 4
```

## Install dependency check (Binary)
```bash
apt update
apt install openjdk-8-jre -y
VERSION=$(curl -s https://jeremylong.github.io/DependencyCheck/current.txt)
curl -Ls "https://github.com/jeremylong/DependencyCheck/releases/download/v$VERSION/dependency-check-$VERSION-release.zip" --output dependency-check.zip
unzip dependency-check.zip -d /opt/
```

Add tool to PATH
```bash
export PATH=/opt/dependency-check/bin:$PATH
dependency-check.sh -h
```

## Run dependency check 
```bash 
dependency-check.sh --scan /webapp --format "JSON" --project "Webgoat" --nvdApiKey "########-####-####-####-############" --out /opt
```

To fail on CVSS more than 4 
```bash
dependency-check.sh --scan /webapp --format "JSON" --project "Webgoat" --nvdApiKey "########-####-####-####-############" --failOnCVSS 4 --out /opt
```

## Perform supression
Use the available parameter in the tool to mark the CVE-2021-23358 issue as a false positive.
```bash
vi suppression.xml
```

```xml
<?xml version="1.0" encoding="UTF-8"?>
<suppressions xmlns="https://jeremylong.github.io/DependencyCheck/dependency-suppression.1.3.xsd">
    <suppress>
        <notes><![CDATA[
        Ignore underscore-min.js issue as FP
        ]]></notes>
        <vulnerabilityName>CVE-2021-23358</vulnerabilityName>
    </suppress>
</suppressions>
```

### Re-run dependency check with supression 
```bash
dependency-check.sh --scan /webapp --format "JSON" --project "Webgoat" --nvdApiKey "########-####-####-####-############" --failOnCVSS 4 --suppression suppression.xml --out /opt
```

## Run dependency check in gitlab pipeline
### Option 1 (Follow course)
Bash script provided by course (run-depcheck.sh) 
```bash
#!/bin/sh
#run-depcheck.sh 

DATA_DIRECTORY="$PWD/data"
REPORT_DIRECTORY="$PWD/reports"

if [ ! -d "$DATA_DIRECTORY" ]; then
  echo "Initially creating persistent directories"
  mkdir -p "$DATA_DIRECTORY"
  chmod -R 777 "$DATA_DIRECTORY"

  mkdir -p "$REPORT_DIRECTORY"
  chmod -R 777 "$REPORT_DIRECTORY"
fi

docker run --rm \
  --volume $(pwd):/src \
  --volume "$DATA_DIRECTORY":/usr/share/dependency-check/data \
  --volume "$REPORT_DIRECTORY":/reports \
  owasp/dependency-check \
  --scan /src \
  --format "JSON" \
  --project "Webgoat" \
  --failOnCVSS 4 \
  --nvdApiKey "########-####-####-####-############"
  --out /reports
```

Check in run-depcheck.sh into repository and edit .gitlab-ci.yml accordingly

```yaml
image: docker:20.10

services:
  - docker:dind

stages:
  - test

odc-backend:
  stage: test
  image: gitlab/dind:latest
  script:
    - chmod +x ./run-depcheck.sh
    - ./run-depcheck.sh
  artifacts:
    paths:
      - reports/dependency-check-report.json
    when: always
    expire_in: one week
  allow_failure: true
```

### Option 2 (Run docker commands directly)
```yaml 
image: docker:20.10

services:
  - docker:dind

stages:
  - test

odc-backend:
  stage: test
  script:
    - docker run --rm --volume $(pwd):/src --volume $(pwd)/data:/usr/share/dependency-check/data owasp/dependency-check --scan /src --format "JSON" --project "Webgoat" --out /src --nvdApiKey "########-####-####-####-############" --nvdApiDelay 5000
  artifacts:
    paths:
      - reports/dependency-check-report.json
    when: always
    expire_in: one week
  allow_failure: true
```

## Troubleshooting
```
Failed to process CVE-2024-0069
org.owasp.dependencycheck.data.nvdcve.DatabaseException: Error updating 'CVE-2024-0069'
	at org.owasp.dependencycheck.data.nvdcve.CveDB.updateVulnerability(CveDB.java:877)
	at org.owasp.dependencycheck.data.update.nvd.api.NvdApiProcessor.call(NvdApiProcessor.java:98)
	at org.owasp.dependencycheck.data.update.nvd.api.NvdApiProcessor.call(NvdApiProcessor.java:33)
	at java.base@17.0.9/java.util.concurrent.FutureTask.run(FutureTask.java:264)
	at java.base@17.0.9/java.util.concurrent.ThreadPoolExecutor.runWorker(ThreadPoolExecutor.java:1136)
	at java.base@17.0.9/java.util.concurrent.ThreadPoolExecutor$Worker.run(ThreadPoolExecutor.java:635)
	at java.base@17.0.9/java.lang.Thread.run(Thread.java:840)
Caused by: org.h2.jdbc.JdbcSQLNonTransientConnectionException: The database has been closed [90098-214]
	at org.h2.message.DbException.getJdbcSQLException(DbException.java:678)
	at org.h2.message.DbException.getJdbcSQLException(DbException.java:477)
	at org.h2.message.DbException.get(DbException.java:212)
	at org.h2.engine.SessionLocal.getTransaction(SessionLocal.java:1596)
	at org.h2.engine.SessionLocal.getStatementSavepoint(SessionLocal.java:1606)
	at org.h2.engine.SessionLocal.setSavepoint(SessionLocal.java:848)
	at org.h2.command.Command.executeUpdate(Command.java:244)
	at org.h2.jdbc.JdbcPreparedStatement.executeUpdateInternal(JdbcPreparedStatement.java:209)
	at org.h2.jdbc.JdbcPreparedStatement.executeUpdate(JdbcPreparedStatement.java:169)
	at org.apache.commons.dbcp2.DelegatingPreparedStatement.executeUpdate(DelegatingPreparedStatement.java:137)
	at org.apache.commons.dbcp2.DelegatingPreparedStatement.executeUpdate(DelegatingPreparedStatement.java:137)
	at org.owasp.dependencycheck.data.nvdcve.CveDB.deleteVulnerability(CveDB.java:1113)
	at org.owasp.dependencycheck.data.nvdcve.CveDB.updateVulnerability(CveDB.java:862)
	... 6 more
Caused by: org.h2.mvstore.MVStoreException: Reading from file sun.nio.ch.FileChannelImpl@68eb327b failed at 8265648 (length -1), read 0, remaining 24576 [2.1.214/1]
	at org.h2.mvstore.DataUtils.newMVStoreException(DataUtils.java:1004)
	at org.h2.mvstore.DataUtils.readFully(DataUtils.java:470)
	at org.h2.mvstore.FileStore.readFully(FileStore.java:98)
	at org.h2.mvstore.Chunk.readBufferForPage(Chunk.java:422)
	at org.h2.mvstore.MVStore.readPage(MVStore.java:2569)
	at org.h2.mvstore.MVMap.readPage(MVMap.java:633)
	at org.h2.mvstore.Page$NonLeaf.getChildPage(Page.java:1125)
	at org.h2.mvstore.CursorPos.traverseDown(CursorPos.java:61)
	at org.h2.mvstore.MVMap.operate(MVMap.java:1770)
	at org.h2.mvstore.MVMap.put(MVMap.java:156)
	at org.h2.mvstore.MVStore.createChunk(MVStore.java:1622)
	at org.h2.mvstore.MVStore.serializeAndStore(MVStore.java:1595)
	at org.h2.mvstore.MVStore.lambda$storeNow$4(MVStore.java:1518)
	at java.base/java.util.concurrent.Executors$RunnableAdapter.call(Executors.java:539)
	... 4 more
Caused by: java.nio.channels.ClosedChannelException
	at java.base/sun.nio.ch.FileChannelImpl.ensureOpen(FileChannelImpl.java:159)
	at java.base/sun.nio.ch.FileChannelImpl.read(FileChannelImpl.java:814)
	at org.h2.mvstore.DataUtils.readFully(DataUtils.java:456)
	... 16 more
```
When face similar error as above or keep getting HTTP 503 reply, there is a chance API key has been invalidated. 

### Check if NVD API Key is still valid 
```bash
curl -H "Accept: application/json" -H "apiKey: ########-####-####-####-############" -v https://services.nvd.nist.gov/rest/json/cves/2.0\?cpeName\=cpe:2.3:o:microsoft:windows_10:1607:\*:\*:\*:\*:\*:\*:\*
```
If no JSON is returned and you see a 404 error the API Key is invalid and you should request a new one.

Reference: https://github.com/jeremylong/Open-Vulnerability-Project/tree/main/vulnz#caching-the-nvd-cve-data