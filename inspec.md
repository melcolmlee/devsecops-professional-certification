# Inspec

## Install inspec
```bash
wget https://packages.chef.io/files/stable/inspec/4.37.8/ubuntu/18.04/inspec_4.37.8-1_amd64.deb
dpkg -i inspec_4.37.8-1_amd64.deb
```

This command can also be used but might require free license registration
```bash
curl https://omnitruck.chef.io/install.sh | sudo bash -s -- -v 5.22.36 -P inspec
```

## Prevents ssh agent from prompting YES or NO question when running command
```bash
echo "StrictHostKeyChecking accept-new" >> ~/.ssh/config
```

## Run the linux-baseline Inspec profile against the production server
```bash
inspec exec https://github.com/dev-sec/linux-baseline -t ssh://root@prod-xxxx -i ~/.ssh/id_rsa --chef-license accept
```

## Create a custom inspec profile named ubuntu
```bash
inspec init profile ubuntu --chef-license accept
```

## Add new inspec task to file at ubuntu/controls/example.rb for custom profile
Code basically checks whether shadow file is owned by root or not
```
vi ubuntu/controls/example.rb
```

```rb
describe file('/etc/shadow') do
    it { should exist }
    it { should be_file }
    it { should be_owned_by 'root' }
  end
```

## Validate custom profile to make sure there are no syntax errors
```bash
inspec check ubuntu
```

## Run ubuntu custom profile on local machine before executing against prod server
```bash
inspec exec ubuntu
```

## Run the custom ubuntu profile created against prod server
```bash
inspec exec ubuntu -t ssh://root@prod-xxxx -i ~/.ssh/id_rsa --chef-license accept
```

## Edit inspec profile to add four basic checks (system, password, ssh, and useradd) from PCI/DSS requirements
```bash
vi /inspec-profile/controls/example.rb
```

```rb
describe file('/etc/pam.d/system-auth') do
    its('content') { 
        should match(/^\s*password\s+requisite\s+pam_pwquality\.so\s+(\S+\s+)*try_first_pass/)
    }
    its('content') {
        should match(/^\s*password\s+requisite\s+pam_pwquality\.so\s+(\S+\s+)*retry=[3210]/)
    }
end

describe file('/etc/pam.d/password-auth') do
    its('content') { 
        should match(/^\s*password\s+requisite\s+pam_pwquality\.so\s+(\S+\s+)*try_first_pass/)
    }
    its('content') {
        should match(/^\s*password\s+requisite\s+pam_pwquality\.so\s+(\S+\s+)*retry=[3210]/)
    }
end

describe file('/etc/default/useradd') do
    its('content') {
        should match(/&\s*INACTIVE\s*=\s*(30|[1-2][0-9]|[1-9])\s*(\s+#.*)?$/)
    }
end

describe file('/etc/ssh/sshd_config') do
    it { should exist }
    it { should be_file }
    it { should be_owned_by 'root' }
    its('content') { should match 'PasswordAuthentication no' }
end
```
## Edit inspec profile to check for packages and service
```rb
describe package('clamav') do
    it { should be_installed }
  end

  describe service('clamd') do
    it { should be_enabled }
    it { should be_installed }
    it { should be_running }
  end
```

## Run custom inspec profile in gitlab pipeline
```yaml
image: docker:20.10

services:
  - docker:dind

stages:
  - test

compliance:
  stage: test
  script:
    - docker run -i --rm -v $(pwd):/share chef/inspec check challenge --chef-license accept
    - docker run -i --rm -v $(pwd):/share chef/inspec exec challenge --chef-license accept
  allow_failure: true
```

```yaml
image: docker:20.10

services:
  - docker:dind

stages:
  - prod

inspec:
  stage: prod
  only:
    - "main"
  environment: production
  before_script:
    - mkdir -p ~/.ssh
    - echo "$DEPLOYMENT_SERVER_SSH_PRIVKEY" | tr -d '\r' > ~/.ssh/id_rsa
    - chmod 600 ~/.ssh/id_rsa
    - eval "$(ssh-agent -s)"
    - ssh-add ~/.ssh/id_rsa
    - ssh-keyscan -t rsa $DEPLOYMENT_SERVER >> ~/.ssh/known_hosts
  script:
    - docker run --rm -v ~/.ssh:/root/.ssh -v $(pwd):/share hysnsec/inspec exec https://github.com/dev-sec/linux-baseline -t ssh://root@$DEPLOYMENT_SERVER -i ~/.ssh/id_rsa --chef-license accept --reporter json:inspec-output.json
  artifacts:
    paths: [inspec-output.json]
    when: always
  ```