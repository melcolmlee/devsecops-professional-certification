# DefectDojo
## Download script To Upload Results
curl https://gitlab.practical-devsecops.training/-/snippets/3/raw -o upload-results.py
pip3 install requests
python3 upload-results.py --help

## Inputs/variables to provide for upload-results.py script to work
- **HOST** -	https://dojo-xxxx.lab.practical-devsecops.training

- **USERNAME** - eg. root

- **API_KEY**	- Get it at https://dojo-xxxx.lab.practical-devsecops.training/api/key-v2 or use curl method to get from API (see below)

- **ENGAGEMENT_ID**	- ID of the engagement

- **PRODUCT_ID** - ID of product

- **LEAD_ID** - 	ID of the user conducting the testing

- **ENVIRONMENT** -	Environment name eg. Production

- **SCANNER** -	Name of the scanner, this is case sensitive eg. ZAP Scan, Bandit Scan, etc

- **RESULT_FILE** -	Path to tool's output

## API method to get API key 
```bash
export API_KEY=$(curl -s -XPOST -H 'content-type: application/json' https://dojo-xxxx.lab.practical-devsecops.training/api/v2/api-token-auth/ -d '{"username": "root", "password": "pdso-training"}' | jq -r '.token' )
```

## Upload brakeman’s scan output (brakeman-result.json) to DefectDojo
```bash
python3 upload-results.py --host dojo-hsjzg1g1.lab.practical-devsecops.training --api_key $API_KEY --engagement_id 2 --product_id 3 --lead_id 1 --environment "Production" --result_file brakeman-result.json --scanner "Brakeman Scan"
```

## Upload bandit’s scan output (bandit-output.json) to DefectDojo
```bash
python3 upload-results.py --host dojo-hsjzg1g1.lab.practical-devsecops.training --api_key $API_KEY --engagement_id 1 --product_id 1 --lead_id 1 --environment "Production" --result_file bandit-output.json --scanner "Bandit Scan"
```

## Scan production server with the help of the ZAP docker image
```bash
docker run --user $(id -u):$(id -g) -w /zap -v $(pwd):/zap/wrk:rw -rm softwaresecurityproject/zap-stable:2.13.0 zap-baseline.py -t https://prod-hsjzg1g1.lab.practical-devsecops.training -d -x zap-output.xml
```

## Upload ZAP scan output (zap-output.xml) to Defect Dojo
```bash
python3 upload-results.py --host dojo-hsjzg1g1.lab.practical-devsecops.training --api_key $API_KEY --engagement_id 1 --product_id 1 --lead_id 1 --environment "Production" --result_file /webapp/zap-output.xml --scanner "ZAP Scan"
```

## Update .gitlab-ci.yml file and add the upload-results.py as part of the ZAP Scan in the CI/CD pipeline inside after_script
```yml
dast-zap:
  stage: integration
  before_script:
    - apk add py-pip py-requests
    - docker pull softwaresecurityproject/zap-stable:2.13.0
  script:
    - docker run --user $(id -u):$(id -g) -w /zap -v $(pwd):/zap/wrk:rw --rm softwaresecurityproject/zap-stable:2.13.0 zap-baseline.py -t https://prod-xxxx.lab.practical-devsecops.training -d -x zap-output.xml
  after_script:
    - python3 upload-results.py --host $DOJO_HOST --api_key $DOJO_API_TOKEN --engagement_id 1 --product_id 1 --lead_id 1 --environment "Production" --result_file zap-output.xml --scanner "ZAP Scan"
  artifacts:
    paths: [zap-output.xml]
    when: always
    expire_in: 1 day
```